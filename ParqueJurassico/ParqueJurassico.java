import java.util.Scanner;

public class ParqueJurassico {
    static final String FILENAME = "resultados.txt";
    //Scanner
    static Scanner sc = new Scanner(System.in);

    public static void main(String[] args) throws InterruptedException {
        int nGuias   = 2;
        int nVisitantes = 7;

        System.out.println("Quantos ciclos pretende?");
        int nCiclos   = sc.nextInt();

        Ilha ilha = new Ilha(nGuias, nVisitantes, nCiclos);

        System.out.println("Terminado");
    }

}
