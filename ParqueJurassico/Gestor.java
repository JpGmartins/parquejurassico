import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Gestor extends Apoio {

    //Declaração dos diversos estados
    public final static int DORMINDO = 0;
    public final static int FORMANDO_O_GRUPO = 1;
    public final static int ESPERA_VEICULO = 2;
    public final static int ORGANIZANDO_VIAGEM = 3;

    //Define quando o gestor dorme
    Semaphore dorme = new Semaphore(0);
    // Semaforo do guia
    Semaphore guia = new Semaphore(0);

    public Gestor(Ilha ilha) {
        super(ilha);
    }

    public void run() {
        try {
            while (true) {
                dormindo();
                formando_o_grupo();
                espera_veiculo();
                organizando_viagem();
            }
        } catch (InterruptedException e) {  }
    }




    //HAVIA AQUI UM BUG NA PRIMEIRA VERSÃO QUE NÃO PERMITIA QUE OS VEÍCULOS TIVESSEM 3 PESSOAS
    private void dormindo() throws InterruptedException {
        setEstadoAtual(DORMINDO);
        // desliga o semaforo
        guia.drainPermits();

        ilha.semaforo.release(3);

        dorme.drainPermits();
    }



    private void formando_o_grupo() throws InterruptedException {
        setEstadoAtual(FORMANDO_O_GRUPO);

        System.out.println("À espera de " + ilha.semaforo.availablePermits() + " visitantes");
        // espera 500 por novos visitantes
        dorme.tryAcquire(500, TimeUnit.MILLISECONDS);

        // Reset ao semaforo
        ilha.semaforo.drainPermits();
     }

    private void espera_veiculo() throws InterruptedException {
        setEstadoAtual(ESPERA_VEICULO);
        if (!Guia.semaforo.hasQueuedThreads())
            guia.acquire();
    }

    private void organizando_viagem() throws InterruptedException {
        setEstadoAtual(ORGANIZANDO_VIAGEM);
        ilha.reset();

        // adiciona os visitantes que estão prontos
        int vAuxiliar = 0;

        for (Visitante visitante : ilha.getVisitantes()) {
            if (visitante.semaforo.hasQueuedThreads()) {
                 ilha.getGrupo()[vAuxiliar++] = visitante;
                 //verifica que não há mais de 3 visitantes por "carro"
                 if(vAuxiliar == 3)
                     break;
            }
        }

        Guia.semaforo.release();
    }

    public String getStringEstado(int estado) {
        switch (estado) {
            case DORMINDO: return " Dormir";
            case FORMANDO_O_GRUPO: return " F_Grupo";//Formar grupo
            case ESPERA_VEICULO:  return " Esp_Carro";//Espera Carro
            case ORGANIZANDO_VIAGEM: return " Org_Viag.";//Organiza Viagem
        }
        return null;
    }
}
