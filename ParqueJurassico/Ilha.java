import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.concurrent.Semaphore;

public class Ilha {

    //Cria os diversos arrays
    private Apoio[] apoios;
    private Visitante[] visitantes;
    private Guia[] guias;
    //array do grupo COM MÁXIMO DE 3 VISITANTES
    private Visitante grupo[] = new Visitante[3];
    private String  log = "";

    //Apenas há um gestor logo não é necessário um array
    private Gestor gestor;

    //declara o semaforo
    Semaphore semaforo;

    private int ciclos = 1;

    public Ilha(int nGuias, int nVisitantes, int nCiclos) {

        // inicia arrays
        visitantes = new Visitante[nVisitantes];
        guias = new Guia[nGuias];

        // prepara os objetos no arrays
        while (nVisitantes > 0) {
            nVisitantes--;
            getVisitantes()[nVisitantes] = new Visitante(this);
        }

        while (nGuias > 0) {
            nGuias--;
            getGuias()[nGuias] = new Guia(this);
        }

        atribuiGestor(new Gestor(this));

        setCiclos(nCiclos);
        // semaforo principal fifo
        semaforo = new Semaphore(0, true);

        //cria um array com as threads
        apoios = new Apoio[getVisitantes().length + getGuias().length + 1];
        System.arraycopy(getVisitantes(), 0, apoios, 0, getVisitantes().length);
        System.arraycopy(getGuias(), 0, apoios, getVisitantes().length, getGuias().length);
        apoios[apoios.length - 1] = getGestor();

        iniciar();
    }

    private void iniciar() {

        // inicia as threads
        getGestor().inicia();
        for (Apoio apoio : getVisitantes())
            apoio.inicia();

        for (Apoio apoio : getGuias())
            apoio.inicia();

        Thread athread;
        athread = iniciarThread();

        //Espera que a thread do visitante termine para terminar as restantes
        try {
            for (Apoio apoio : getVisitantes()) {
                apoio.join();
            }
            for (Apoio apoio : getGuias()) {
                apoio.interrupt();
            }

            gestor.interrupt();

        } catch (InterruptedException e) {
        } finally {
            try {
                PrintWriter saida = new PrintWriter(ParqueJurassico.FILENAME);
                saida.println(log);
                saida.close();
            }catch (FileNotFoundException e){} finally {

            }

        }
        athread.interrupt();
    }

    // "impressora"
    private Thread iniciarThread() {
        Thread thread = new Thread(() -> {
            try {
                while (true) {
                    //Frequência de impressão dos resultados
                    Thread.sleep(20);

                    for (Apoio apoio : getPersons())
                        System.out.println(apoio);

                    System.out.println("________________________________________");
                    System.out.println(" ");
                }
            } catch (InterruptedException e) {
            }
        });

        thread.start();

        return thread;
    }

    void estadoLog() {

        String line = "";

        synchronized(this) {

            // Report header
            if (log.isEmpty()) {
                for (Apoio apoio : getPersons()) {
                    line += String.format("%1$-13s", apoio.getTipo() + " " + apoio.getId());
                }
                line += "\r\n";
            }

            // Report body
            for (Apoio apoio : getPersons()) {
                line += String.format("%1$-13s", apoio.getStringEstado());
            }

            log += line + "\r\n";
        }
    }

    // visitantes entram na fila de espera
    void esperaNaFila(Visitante visitante) throws InterruptedException {
        semaforo.acquire();
        acordaGerente(visitante);
    }

    // Guides inicia vizita
    synchronized void entrar(Guia guia){
        for (Visitante visitante : getGrupo()) {
            if (visitante != null) {
                visitante.setEstadoAtual(Visitante.ENTRANDO);
                visitante.setGuia(guia);
                visitante.semaforo.release();
            }
        }

    }

    // Guides termina viagem
    synchronized void sair(Guia guia) {
        //ativa visitantes em espera
        synchronized (guia) {
            guia.notifyAll();
        }
        acordaGerente(guia);
    }

    // acorda gerente
    void acordaGerente(Apoio apoio) {
        switch (apoio.getTipo()) {
            case "Guia":
                gestor.guia.release();
                break;
            case "Visitante":
                gestor.dorme.release();
                break;
        }
    }


    void estado() {
        String linha = "";

        synchronized (this) {
            for (Apoio apoio : getPersons()) {
                linha += String.format("%1$-13s", apoio.getStringEstado());
            }

        }
    }

    public Gestor getGestor() {
        return gestor;
    }

    public void atribuiGestor(Gestor gestor) {
        this.gestor = gestor;
    }

    public Apoio[] getPersons() {
        return apoios;
    }

    public Visitante[] getVisitantes() {
        return visitantes;
    }

    public Guia[] getGuias() {
        return guias;
    }

    public Visitante[] getGrupo() {
        return grupo;
    }

    public void reset() {
        Visitante grupo[] = new Visitante[3];
    }

    public int getCiclos() {
        return ciclos;
    }

    public void setCiclos(int ciclos) {
        this.ciclos = ciclos;
    }

}
