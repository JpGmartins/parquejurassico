import java.util.concurrent.Semaphore;

public class Guia extends Apoio {

    public final static int ADORMIR = 0;
    public final static int ATRABALHAR = 1;

    // Semaforo do gestor
    static Semaphore semaforo = new Semaphore(0);

    public Guia(Ilha ilha) {
        super(ilha);
    }

    public void run() {
        try {
            while(true) {
                aDormir();
                aEntrar();
                emVisita();
                aSair();
            }
        } catch(InterruptedException e) { }
    }

    private void emVisita() throws InterruptedException {
        setEstadoAtual(Guia.ATRABALHAR);
        sleep(Apoio.numAleatorio());
    }

    private void aEntrar() throws InterruptedException {
        ilha.entrar(this);
    }

    private void aDormir() throws InterruptedException {
        setEstadoAtual(Guia.ADORMIR);
        Guia.semaforo.acquire();
    }

    private void aSair() throws InterruptedException {

        ilha.sair(this);
    }

    public String getStringEstado(int estado) {
        switch(estado) {
            case ADORMIR:
                return " Aguarda";
            case ATRABALHAR:
                return " Trabalhar";
        }
        return null;
    }

}
