abstract public class Apoio extends Thread {

    protected Ilha ilha = null;

    private int estadoPersonalizado = 0;

    public Apoio(Ilha ilha) {

        this.ilha = ilha;
    }

    static int numAleatorio()
    {
        int min = 50;
        int max = 100;
        int alcance = (max - min) + 1;
        return (int)(Math.random() * alcance) + min;
    }

    public void inicia() {
        super.start();
    }

    void dorme() throws InterruptedException {
        sleep(numAleatorio());
    }

    final int getEstadoAtual() {
        return estadoPersonalizado;
    }

    void setEstadoAtual(int state) {
        estadoPersonalizado = state;
        ilha.estadoLog();
    }

    public String getStringEstado() {
        return getStringEstado(getEstadoAtual());
    }

    public String getTipo() {
        return this.getClass().toString().replace("class", "").trim();
    }

    protected abstract String getStringEstado(int state);

    public String toString() {
        return String.format("%1$10s", getTipo())+ " " + getId() + this.getStringEstado();
    }
}
