import java.util.concurrent.Semaphore;

public class Visitante extends Apoio {
    public final static int CHEGANDO = 0;
    public final static int DICIDINDO = 1;
    public final static int VISITANDO_MUSEU = 2;
    public final static int ESPERANDO = 3;
    public final static int ENTRANDO = 4;
    public final static int EM_ATIVIDADE = 5;
    public final static int ASAIR = 6;

    private Guia guia;

    // SEMAFORO DO GUIA
    Semaphore semaforo = new Semaphore(0);

    public Visitante(Ilha ilha) {
        super(ilha);
    }

    public void run() {
        try {
            chegando();
            dicidindo();

            int ciclos = ilha.getCiclos();

            while (ciclos > 0) {
                ciclos --;

                visitaMuseu();
                espera();
                entrando();
                visitando();
            }
        } catch(InterruptedException e) {
        } finally {
            leave();
        }

    }

    private void chegando() throws InterruptedException {
        setEstadoAtual(CHEGANDO);
        dorme();
    }

    private void dicidindo() throws InterruptedException  {
        setEstadoAtual(DICIDINDO);
        dorme();
    }

    private void visitaMuseu() throws InterruptedException {
        setEstadoAtual(VISITANDO_MUSEU);
        dorme();
    }

    private void espera() throws InterruptedException {
        setEstadoAtual(Visitante.ESPERANDO);
        ilha.esperaNaFila(this);
    }

    private void entrando() throws InterruptedException {
        System.out.println("Visitante " + getId() + " está pronto.");
        semaforo.acquire();
    }

    private void visitando() throws InterruptedException {
        setEstadoAtual(EM_ATIVIDADE);

        synchronized (guia) {
            guia.wait();
        }

        guia = null;
        semaforo.drainPermits();
    }

    private void leave() {
        setEstadoAtual(ASAIR);
    }

    public Guia getGuia() {
        return guia;
    }

    public void setGuia(Guia guia) {
        this.guia = guia;
    }

    public String getStringEstado(int estado) {
        switch(estado) {
            case CHEGANDO:
                return " Chegar";
            case DICIDINDO:
                return " Decidir";
            case VISITANDO_MUSEU:
                return " Museu";
            case ESPERANDO:
                return " Aguardar";
            case ENTRANDO:
                return " Agrupar";
            case EM_ATIVIDADE:
                return " VISITA "+getGuia().getId();
            case ASAIR:
                return " Sair";
        }
        return null;
    }

    public String toString() {

        String str = (getGuia() != null)?" -> Guia " + getGuia().getId():"";

        str += (semaforo.hasQueuedThreads())?" -> Está pronto":"";

        return super.toString() + str;

    }
}
